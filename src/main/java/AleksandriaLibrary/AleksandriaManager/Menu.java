package AleksandriaLibrary.AleksandriaManager;
import AleksandriaLibrary.Items.Add.AddBook;
import AleksandriaLibrary.Items.Add.AddVinyl;
import AleksandriaLibrary.Items.Item.ItemManager;

import java.util.Scanner;

import static AleksandriaLibrary.Person.CreateAccount.createAccount;

public class Menu {

    public void printMenu() {
        AddBook addBook = new AddBook();
        ItemManager itemManager = new ItemManager();
        System.out.println("What you want to do ??\n" +
                "Press [ 1 ] to add item\n" +
                "Press [ 2 ] to delete item\n" +
                "Press [ 3 ] to review resources\n" +
                "Press [ 4 ] to back\n" +
                "Press [ 0 ] to exit\n");

        int decision = 1;
        while (decision > 0) {
            Scanner scanner = new Scanner(System.in);
            decision = scanner.nextInt();
            switch (decision) {
                case 1:
                    addItem();
                    backToMainMenu();
                    break;
                case 2:
                    itemManager.removeItem();
                    backToMainMenu();
                    break;
                case 3:
                    itemManager.printItemList();
                    backToMainMenu();
                    break;
                case 4:
                    createAccount();
                    backToMainMenu();
                    break;
                case 0:
                    decision = 0;
                    break;
            }
        }
    }

    public static void addItem() {
        AddBook addBook = new AddBook();
        Scanner scanner = new Scanner(System.in);
        System.out.println("1- Book\n2- Vinyl");
        int choise = scanner.nextInt();
        if (choise == 1) {
            addBook.addBook();
        }
        if (choise == 2) {
            AddVinyl.addVinyl();
        }
    }

    private static void backToMainMenu() {
        System.out.println("\nPress 1 to return to the menu");
        int decision = 1;
        while (decision > 0) {
            Scanner scanner = new Scanner(System.in);
            decision = scanner.nextInt();
            switch (decision) {
                case 1:
                    Menu menu = new Menu();
                    menu.printMenu();
            }
        }
    }
}