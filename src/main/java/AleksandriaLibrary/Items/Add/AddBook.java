package AleksandriaLibrary.Items.Add;
import AleksandriaLibrary.Items.Item.BookCategory;
import AleksandriaLibrary.Items.Item.ItemManager;
import java.util.Scanner;

public class AddBook extends ItemManager {

    public void addBook() {
        //pobieram dane potrzebne do dodania książki od użytkownika
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter the tittle of the book");
        String tittle = scanner.nextLine();
        System.out.println("Please enter the author of the book");
        String author = scanner.nextLine();
        System.out.println("Plese enter the number of pages");
        int size = scanner.nextInt();
        System.out.println("Plese chose category of your film:\n" +
                "    Sience_Fiction= 1 \n" +
                "    DRAMA= 2\n" +
                "    ACTION= 3\n" +
                "    ADVENTURE= 4\n" +
                "    COMEDY= 5,\n" +
                "    HORROR= 6,\n" +
                "    THRILLER= 7");
        BookCategory type = null;
        int option = scanner.nextInt();
        switch (option) {
            case 1:
                 type = BookCategory.SIENCE_FICTION;
                break;
            case 2:
                type = BookCategory.DRAMA;
                break;
            case 3:
                type = BookCategory.ACTION;
                break;
            case 4:
                type = BookCategory.ADVENTURE;
                break;
            case 5:
                type = BookCategory.COMEDY;
                break;
            case 6:
                type = BookCategory.HORROR;
                break;
            case 7:
                type = BookCategory.THRILLER;
                break;
            default:
                System.out.println("Please Try Again");

        }
        //stworzenie i dodanie ksiazki do listy za pomocą metody addBookToItemList
        addBookToItemList(tittle,author,size,type);
        System.out.println("Your book is create ! Thank's :)");
        printItemList();

    }

}

