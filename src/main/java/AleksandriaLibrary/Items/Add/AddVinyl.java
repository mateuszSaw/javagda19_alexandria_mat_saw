package AleksandriaLibrary.Items.Add;

import AleksandriaLibrary.Items.Item.Item;
import AleksandriaLibrary.Items.Item.Vinyl;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class AddVinyl {

    public static void addVinyl(){
        Scanner scanner = new Scanner(System.in);
        List<Item> itemLiist = new ArrayList<>();
        System.out.println("Please enter the tittle of the Vinyl");
        String tittle = scanner.nextLine();

        System.out.println("Please enter the author of the Vinyl");
        String author = scanner.nextLine();
        Vinyl vinyl = new Vinyl(tittle,author);
        itemLiist.add(vinyl);
    }
}
