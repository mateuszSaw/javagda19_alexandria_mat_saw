package AleksandriaLibrary.Items.Item;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ItemManager {

//Lista wszystkich itemow ( book, vinyl )
    private List<Item> items;

    //konstruktor listy
    public ItemManager() {
        this.items = createItem();
    }
    //nie wiem co to ale z tym dopiero dziala lista "items"
    public List<Item> findItem() {
        return items;
    }

    //drukowanie listy itemow
    public void printItemList() {
        List<Item> items = findItem();
        for (Item itemsToPrint : items) {
            System.out.println(itemsToPrint);
        }
    }

    //metoda dodadnia ksiazki do listy itemow
    public void addBookToItemList(String tittle,String author,int size, BookCategory type) {
        Item book = new Book(tittle, author, size, type);
        items.add(book);

}

    //usuwanie dowolnego itema
    public  void deleteItem( int itemID){
     for (int i = 0; i < items.size(); i++){
         Item item = items.get(i);
         if (item.getItemID() == itemID) {
             items.remove(i);
         }
     }}

        public void removeItem (){
            Scanner scanner = new Scanner(System.in);
            System.out.println("Please enter the id of the item you want to delete");
            printItemList();
            int id = scanner.nextInt();
            deleteItem(id);
            printItemList();
            System.out.println("Item deleted");
        }


    // moja lista itemow z kilkoma dodanymi ksiazkami
    public List<Item> createItem () {
        List<Item> items = new ArrayList<>();
        items.add(new Book("The Godfather", "Maro Puzzo", 450, BookCategory.THRILLER));
        items.add(new Book("Harry Potter and Azkaban Prisoner", "J.K. Rowling", 450, BookCategory.THRILLER));
        items.add(new Book("Być Liderem", "Alex Ferguson", 450, BookCategory.THRILLER));
        items.add(new Book("The Godfather II", "Maro Puzzo", 450, BookCategory.THRILLER));

        return items;
    }
}