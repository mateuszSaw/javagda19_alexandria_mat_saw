package AleksandriaLibrary.Items.Item;

import com.sun.javafx.binding.StringFormatter;

public class Book extends Item {
    private int size;
    private BookCategory type;

    public Book(String tittle, String author, int size, BookCategory type) {
        super(tittle, author);
        this.size = size;
        this.type = type;
    }


    public int getSize() {
        return size;
    }

    public BookCategory getType() {
        return type;
    }

    @Override
    public String toString() { return super.toString() + " | " + type + " | " + size;
    }
}






