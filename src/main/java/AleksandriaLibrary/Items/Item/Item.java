package AleksandriaLibrary.Items.Item;
import java.util.ArrayList;
import java.util.List;
public abstract class Item {
    private static int COUNT = 1;

    private int itemID;
    private String tittle;
    private  String author;

//    private String description;

    public Item(String tittle, String author) {
    this.itemID = COUNT++;
    this.tittle = tittle;
    this.author = author;

    //  this.description = description;
}

    public int getItemID() { return itemID; }

    public String getTittle() { return tittle; }

    public String getAuthor() { return author; }

    @Override
    public String toString() { return itemID + " | " + tittle + " | " + author ;
    }
}

